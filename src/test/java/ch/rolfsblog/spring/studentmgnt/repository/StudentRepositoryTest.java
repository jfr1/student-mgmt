package ch.rolfsblog.spring.studentmgnt.repository;

import ch.rolfsblog.spring.studentmgnt.model.Student;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@DisplayName("StudentRepository Test")
public class StudentRepositoryTest {

    @Autowired
    private StudentRepository studentRepository;

    @Test
    @DisplayName("Find Student by Id")
    public void findStudentById() {
        Optional<Student> optionalStudent = studentRepository.findById(1L);
        assertTrue(optionalStudent.isPresent());
    }

}
