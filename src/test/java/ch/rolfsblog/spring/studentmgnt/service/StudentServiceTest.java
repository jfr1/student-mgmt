package ch.rolfsblog.spring.studentmgnt.service;

import ch.rolfsblog.spring.studentmgnt.model.Student;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@SpringBootTest
@DisplayName("StudentService Test")
public class StudentServiceTest {

    @Autowired
    private StudentService studentService;

    @Test
    @DisplayName("Add Student")
    public void addStudent() throws StudentAlreadyExistsException {
        Student student = new Student();
        student.setFirstName("Max");
        student.setLastName("Häberli");
        student.setEmail("max.haeberli@demo.ch");

        studentService.addStudent(student);
    }

    @Test
    @DisplayName("Find Student by Id 1")
    public void findStudent() throws StudentNotFoundException {
        Student student = studentService.findStudent(1L);
        assertNotNull(student);
        assertEquals("Hans", student.getFirstName());
        assertEquals("Muster", student.getLastName());
    }

    @Test
    @DisplayName("Find Student with unknown Id. Should throw StudentNotFoundException")
    public void findBookThrowsBookNotFoundException() {
        assertThrows(StudentNotFoundException.class, () -> studentService.findStudent(4711L));
    }

    @Test
    @DisplayName("Find all Students")
    public void findStudents() {
        List<Student> students = studentService.findStudents();

        assertFalse(students.isEmpty());
        assertEquals(studentService.count(), students.size());
    }
}
