package ch.rolfsblog.spring.studentmgnt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentMgntApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentMgntApplication.class, args);
	}

}
