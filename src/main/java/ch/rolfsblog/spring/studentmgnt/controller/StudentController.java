package ch.rolfsblog.spring.studentmgnt.controller;

import ch.rolfsblog.spring.studentmgnt.model.Student;
import ch.rolfsblog.spring.studentmgnt.service.StudentAlreadyExistsException;
import ch.rolfsblog.spring.studentmgnt.service.StudentNotFoundException;
import ch.rolfsblog.spring.studentmgnt.service.StudentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class StudentController {

    private StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/")
    String getCustomers(Model model) {
        List<Student> students = studentService.findStudents();
        model.addAttribute("students", students);
        return "studentlist";
    }

    @RequestMapping(value = "/delete/{id}")
    String deleteStudent(@PathVariable(name = "id") Long id) throws StudentNotFoundException {
        studentService.deleteStudent(id);
        return "redirect:/";
    }

    @RequestMapping(value = "/add")
    public String showAddStudentForm(Model model) {
        Student student = new Student();
        model.addAttribute("student", student);
        return "addstudent";
    }

    @RequestMapping(value = "/edit/{id}")
    public ModelAndView showEditStudentForm(@PathVariable(name = "id") Long id) throws StudentNotFoundException {
        ModelAndView modelAndView = new ModelAndView("editstudent");
        Student student = studentService.findStudent(id);
        modelAndView.addObject("student", student);
        return modelAndView;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String SaveStudent(@ModelAttribute("student") Student student, @RequestParam(value="action", required=true) String action) throws StudentAlreadyExistsException {
        if (action.equals("save")) {
            studentService.addStudent(student);
        }

        return "redirect:/";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateStudent(@ModelAttribute("student") Student student, @RequestParam(value="action", required=true) String action) throws StudentAlreadyExistsException, StudentNotFoundException {
        if (action.equals("update")) {
            studentService.updateStudent(student);
        }

        return "redirect:/";
    }

}