package ch.rolfsblog.spring.studentmgnt.service;


import ch.rolfsblog.spring.studentmgnt.model.Student;
import ch.rolfsblog.spring.studentmgnt.repository.StudentRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.List;
import java.util.Optional;
import java.util.Set;


@Service
public class StudentService {

    private StudentRepository studentRepository;
    private Validator validator;

    public StudentService(StudentRepository studentRepository, Validator validator) {
        this.studentRepository = studentRepository;
        this.validator = validator;
    }

    @Transactional(rollbackFor = StudentAlreadyExistsException.class)
    public void addStudent(Student student) throws StudentAlreadyExistsException {
        if (studentRepository.findByEmail(student.getEmail()).isPresent()) {
            throw new StudentAlreadyExistsException();
        }
        studentRepository.save(student);
    }

    public Student findStudent(Long id) throws StudentNotFoundException {
        Optional<Student> student = studentRepository.findById(id);
        return student.orElseThrow(StudentNotFoundException::new);
    }

    public List<Student> findStudents() {
        List<Student> students = studentRepository.findAll();
        return students;
    }

    @Transactional(rollbackFor = StudentNotFoundException.class)
    public Student updateStudent(Student student) throws StudentNotFoundException {
        studentRepository.findById(student.getId()).orElseThrow(StudentNotFoundException::new);
        return studentRepository.save(student);
    }

    private boolean isValid(Student student) {
        Set<ConstraintViolation<Student>> constraintViolations = validator.validate(student);
        return constraintViolations.isEmpty();
    }

    public void deleteStudent(Long id) throws StudentNotFoundException {
        Student book = studentRepository.findById(id).orElseThrow(StudentNotFoundException::new);
        studentRepository.delete(book);
    }

    public Long count() {
        return studentRepository.count();
    }
}