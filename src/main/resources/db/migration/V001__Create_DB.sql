DROP TABLE IF EXISTS  student CASCADE;
CREATE SEQUENCE student_sequence INCREMENT BY 1 ;

create table student
(
    id            bigint default student_sequence.nextval primary key,
    address       varchar(255),
    city          varchar(255),
    date_of_birth date,
    email         varchar(255),
    first_name    varchar(255),
    last_name     varchar(255),
    zip           varchar(255),
    UNIQUE (email)
);

insert into student (first_name, last_name, email, date_of_birth, address, zip, city )
values ('Hans', 'Muster', 'hans.muster@demo.ch', '1996-06-12', 'Musterstrasse 21', '3000', 'Bern');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Rosa', 'Meier', 'rosa.meier@demo.ch', '1999-04-25', 'Rosenweg 456', '2501', 'Biel');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Ida', 'Schneider', 'ida.schneider@demo.ch', '1995-07-23', 'Bachstrasse 2', '8000', 'Zürich');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Otto', 'Mars', 'otto.mars@demo.ch', '2000-01-02', 'Quai 15', '2502', 'Biel');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Theo', 'Beringer', 't.beringer@demo.ch', '1998-11-12', 'Uferweg 99', '2500', 'Biel');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Lindsey', 'Craft', 'lindsey.craft@hotma1l.us', '1962-04-01', 'Coshocton Lane', '3470', 'Snellville');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Aaron', 'McCray', 'aaron.mccray@somema1l.us', '1978-03-08', 'Parker Ridge', '7842', 'Abba');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Kaitlyn', 'Wilder', 'kaitlyn.wilder@ma1lbox.co.uk', '1959-03-06', 'Scout Park', '4700', 'Pridgen');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Sierra', 'Schneider', 'sierra.schneider@gma1l.biz', '1977-05-02', 'Seroco Trail', '0418', 'Villa Rica');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Kaylee', 'Austin', 'kaylee.austin@b1zmail.co.uk', '1981-04-07', 'Lavona Parkway', '0710', 'Cogdell');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Sandra', 'Love', 'sandra.love@gma1l.com', '1984-09-14', 'Kerri Boulevard', '9413', 'Jackson');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Juanita', 'Short', 'juanita.short@yah00.co.uk', '1967-05-02', 'Diagonal Rd', '7475', 'Harrietts Bluff');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Susan', 'James', 'susan.james@somema1l.us', '1982-02-24', 'Spring Parkway', '4774', 'Kennesaw');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Trey', 'Reilly', 'trey.reilly@somema1l.net', '1964-10-13', 'Lost Ave', '1194', 'Woodstock');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Paul', 'Kemp', 'paul.kemp@yah00.net', '1982-08-09', 'Goddard Trail', '8949', 'Lafayette');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Harley', 'Burnett', 'harley.burnett@ma1lbox.com', '1962-03-03', 'Arrowhead Ridge', '2781', 'Brunswick');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Adam', 'Joyce', 'adam.joyce@hotma1l.com', '1978-08-18', 'Dooleys Drv', '2380', 'Jasper');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Eva', 'Everett', 'eva.everett@b1zmail.com', '1978-01-17', 'Cottage Crescent', '0333', 'Wilmington Island');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Eugene', 'Morrison', 'eugene.morrison@hotma1l.us', '1977-12-09', 'Alvin Circle', '3281', 'Lincolnton');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Christy', 'Roth', 'christy.roth@yah00.biz', '1968-04-12', 'Keen Way', '7831', 'Douglasville');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Marcus', 'Little', 'marcus.little@gma1l.net', '1977-01-26', 'Rankin Road', '4419', 'Tallapoosa');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Katherine', 'Lee', 'katherine.lee@somema1l.biz', '1976-03-31', 'Prison Park', '5393', 'Bowens Mill');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Leroy', 'Warren', 'leroy.warren@everyma1l.co.uk', '1967-04-01', 'Downard Street', '2423', 'Fargo');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Donna', 'Fields', 'donna.fields@gma1l.org', '1958-09-01', 'Durban Lane', '9086', 'Hartwell');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Chelsea', 'Beach', 'chelsea.beach@yah00.co.uk', '1973-01-05', 'Eldwood Place', '2992', 'Snellville');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Brandon', 'Zimmerman', 'brandon.zimmerman@hotma1l.org', '1976-01-18', 'Woodland Lane', '7762', 'Suwanee');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Lindsey', 'Stein', 'lindsey.stein@yah00.us', '1972-04-25', 'Cattail Lane', '2867', 'Moultrie');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Tonya', 'Cain', 'tonya.cain@somema1l.biz', '1964-10-17', 'Schuler Street', '1467', 'Enigma');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Sue', 'Marquez', 'sue.marquez@hotma1l.us', '1976-11-12', 'Crestway Ave', '9083', 'Sylvania');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Kathy', 'Sexton', 'kathy.sexton@ma1l2u.biz', '1973-09-14', 'Pinewood Crescent', '8530', 'Adrian');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Jan', 'Bender', 'jan.bender@gma1l.us', '1956-01-25', 'Claysville Path', '3909', 'Talbotton');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Lois', 'Rivers', 'lois.rivers@yah00.co.uk', '1961-07-31', 'Mcconnellsville Ridge', '3293', 'Dawsonville');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Sherri', 'Strickland', 'sherri.strickland@gma1l.org', '1974-01-30', 'Lutz St', '6098', 'Bannockburn');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Kris', 'Gill', 'kris.gill@ma1lbox.net', '1975-10-18', 'Barkley Path', '7854', 'Alamo');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Candy', 'Rutledge', 'candy.rutledge@everyma1l.com', '1964-03-02', 'Galbraith St', '2037', 'Screven');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Jeffery', 'Luna', 'jeffery.luna@ma1l2u.biz', '1957-11-11', 'Adamsville Drive', '7955', 'Milton');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Tyrone', 'Strickland', 'tyrone.strickland@everyma1l.us', '1974-01-02', 'Lincolnway Square', '2758', 'Dover Bluff');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Derek', 'Reeves', 'derek.reeves@yah00.com', '1973-05-25', 'Lisa Court', '6516', 'Fort Valley');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Lois', 'Mills', 'lois.mills@b1zmail.us', '1964-02-16', 'Hartford St', '4610', 'Stilson');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Darla', 'Crawford', 'darla.crawford@somema1l.biz', '1984-08-28', 'Cathy Ridge', '0679', 'Rebecca');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Eva', 'Gibson', 'eva.gibson@b1zmail.com', '1979-01-21', 'Leslie Way', '5551', 'Mcdonough');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Ted', 'Kennedy', 'ted.kennedy@ma1l2u.us', '1976-06-14', 'Chatham Square', '9010', 'Statenville');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Savannah', 'Sheppard', 'savannah.sheppard@hotma1l.org', '1968-12-20', 'Genessee Path', '9938', 'Nankin');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Katrina', 'Carter', 'katrina.carter@everyma1l.com', '1971-04-07', 'Fallsburg Lane', '5984', 'Talbotton');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Madison', 'Cobb', 'madison.cobb@everyma1l.biz', '1981-02-09', 'Dryden Road', '3460', 'Alma');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Jeremiah', 'Cardenas', 'jeremiah.cardenas@everyma1l.com', '1961-10-14', 'Hanover Crescent', '8263', 'Senoia');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Maria', 'Bray', 'maria.bray@somema1l.net', '1973-04-23', 'Blackrun Rd', '5301', 'Nicholls');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Tracey', 'Guzman', 'tracey.guzman@yah00.biz', '1984-11-18', 'Woodland Boulevard', '4209', 'Fairmont');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Abby', 'Hahn', 'abby.hahn@everyma1l.org', '1982-01-01', 'Camp Drv', '7462', 'Fairburn');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Unborn', 'Hawkins', 'unborn.hawkins@gma1l.net', '1972-10-04', 'Claysville Crescent', '4572', 'Hamilton');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Renee', 'Young', 'renee.young@everyma1l.biz', '1968-06-12', 'Franklin Square', '6646', 'Harrietts Bluff');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Leonard', 'Nguyen', 'leonard.nguyen@gma1l.org', '1966-08-21', 'Schneider Street', '5656', 'Graham');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Kelsey', 'Little', 'kelsey.little@ma1lbox.biz', '1958-07-31', 'Richman Place', '9171', 'Clyatteville');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Dave', 'Dillon', 'dave.dillon@somema1l.co.uk', '1965-12-04', 'Gray Street', '0532', 'Ringgold');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Dana', 'Norman', 'dana.norman@gma1l.biz', '1961-05-24', 'Boston Drive', '4934', 'Waterloo');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Rose', 'Glass', 'rose.glass@hotma1l.com', '1965-12-03', 'Spring Heights', '9270', 'Shellman Bluff');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Dan', 'Whitaker', 'dan.whitaker@hotma1l.us', '1978-12-05', 'Fairmount Ave', '7428', 'Enigma');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Sydney', 'Fry', 'sydney.fry@ma1lbox.us', '1966-12-16', 'Quinlan Station', '7122', 'Americus');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Felicia', 'Newman', 'felicia.newman@everyma1l.biz', '1979-09-25', 'Westmoor St', '8595', 'Stockton');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Natasha', 'Knight', 'natasha.knight@ma1l2u.org', '1968-12-07', 'Larry Ave', '7277', 'Pembroke');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Lori', 'Atkins', 'lori.atkins@ma1lbox.us', '1968-02-16', 'Kennedy Lane', '6201', 'Carrollton');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Jodi', 'Carroll', 'jodi.carroll@ma1lbox.biz', '1976-11-28', 'Roland Heights', '1896', 'Broxton');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Olivia', 'Conway', 'olivia.conway@yah00.org', '1968-05-27', 'Sycamore Crescent', '4373', 'Baxley');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Roberta', 'Jefferson', 'roberta.jefferson@somema1l.com', '1980-10-30', 'Danville Way', '4486', 'Sycamore');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Faith', 'Key', 'faith.key@somema1l.us', '1957-09-10', 'Mckaig Street', '2003', 'Rincon');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('James', 'Rocha', 'james.rocha@everyma1l.us', '1974-02-09', 'Bowser Parkway', '7825', 'Alma');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Dennis', 'Tucker', 'dennis.tucker@yah00.net', '1970-08-03', 'Lewis Trail', '7828', 'Gumbranch');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Vivian', 'Barker', 'vivian.barker@somema1l.us', '1974-08-03', 'Eppley St', '3958', 'Hogansville');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Alexis', 'Newman', 'alexis.newman@gma1l.co.uk', '1957-11-17', 'Springdale Heights', '2433', 'Lexington');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Ralph', 'Ware', 'ralph.ware@everyma1l.us', '1955-05-20', 'Wentz Terrace', '0650', 'Folkston');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Johnathan', 'Wheeler', 'johnathan.wheeler@ma1l2u.us', '1975-05-19', 'Potts Lane', '7951', 'Rentz');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Dylan', 'Christian', 'dylan.christian@somema1l.biz', '1981-08-31', 'Wilkins Street', '6368', 'Country Club Estate');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Trevor', 'Shepherd', 'trevor.shepherd@hotma1l.biz', '1977-04-18', 'Prior Terrace', '6262', 'Waverly');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Shawn', 'Cervantes', 'shawn.cervantes@b1zmail.org', '1977-07-18', 'Hartwell Ave', '7423', 'Chester');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Rodney', 'Owens', 'rodney.owens@hotma1l.us', '1980-09-21', 'Belmont Ave', '8729', 'Withers');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Misty', 'Powers', 'misty.powers@ma1lbox.net', '1971-11-28', 'Quarry Way', '1107', 'Greensboro');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Ginger', 'Torres', 'ginger.torres@yah00.us', '1965-10-02', 'Sugartree St', '1141', 'Sparks');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Chris', 'Dickerson', 'chris.dickerson@somema1l.com', '1959-11-06', 'December Drive', '1047', 'Lawrenceville');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Heidi', 'Mullen', 'heidi.mullen@yah00.biz', '1973-10-05', 'Stiver Way', '1363', 'Monticello');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Tracy', 'Dillon', 'tracy.dillon@somema1l.com', '1976-06-25', 'Rains Court', '9254', 'Vernonburg');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Christoph', 'Kerr', 'christoph.kerr@ma1lbox.co.uk', '1958-03-17', 'Darcie Trail', '3449', 'Cuthbert');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Ginger', 'Branch', 'ginger.branch@ma1l2u.co.uk', '1968-07-20', 'Cottage Station', '0663', 'Daisy');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Corey', 'Thomas', 'corey.thomas@ma1l2u.co.uk', '1969-08-11', 'Kings St', '1272', 'Wrightsville');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Vivian', 'Stokes', 'vivian.stokes@ma1lbox.biz', '1973-11-01', 'Homewood Blvd', '7689', 'Vidalia');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Thelma', 'Nielsen', 'thelma.nielsen@b1zmail.com', '1963-07-11', 'Broadvue St', '8439', 'Temple');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Lewis', 'Woodard', 'lewis.woodard@gma1l.org', '1955-06-13', 'Town Blvd', '2352', 'Attapulgus');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Judy', 'Jacobs', 'judy.jacobs@everyma1l.com', '1979-08-07', 'Meriwether Heights', '3238', 'Jesup');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Ernest', 'Long', 'ernest.long@b1zmail.us', '1963-01-07', 'Henry Ave', '8349', 'Riverdale');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Tamara', 'Day', 'tamara.day@b1zmail.net', '1958-10-14', 'Kossuch Ave', '1655', 'Lyons');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Mike', 'Jensen', 'mike.jensen@yah00.net', '1965-06-11', 'Martinel Heights', '3737', 'Crawfordville');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Zachary', 'Fox', 'zachary.fox@gma1l.org', '1983-02-26', 'Oxford Station', '5399', 'Whigham');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Jade', 'Hayden', 'jade.hayden@gma1l.org', '1964-09-12', 'Ridgeland Lane', '0997', 'Dixie');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Andrea', 'Lamb', 'andrea.lamb@yah00.org', '1959-03-09', 'Rice Station', '5101', 'Unionville');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Patricia', 'Rowe', 'patricia.rowe@hotma1l.com', '1971-05-22', 'Peachblow Heights', '2179', 'Rochelle');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Caleb', 'Galloway', 'caleb.galloway@yah00.com', '1958-01-03', 'Back Way', '5490', 'Macon');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Terri', 'Schultz', 'terri.schultz@gma1l.net', '1962-12-29', 'Eppley Lane', '1267', 'Offerman');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Michelle', 'Odom', 'michelle.odom@yah00.us', '1964-06-07', 'Seborn Road', '4757', 'Funston');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Dillon', 'Grimes', 'dillon.grimes@ma1l2u.biz', '1974-04-16', 'Lewis Boulevard', '2426', 'Newnan');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Angela', 'Lucas', 'angela.lucas@everyma1l.us', '1982-08-06', 'Mundy Station', '5482', 'Villa Rica');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Blake', 'Kent', 'blake.kent@everyma1l.org', '1965-05-23', 'Lincoln Boulevard', '4075', 'Arlington');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Sandy', 'Carter', 'sandy.carter@b1zmail.org', '1962-01-21', 'Discovery Road', '8968', 'Kirkland');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Marion', 'Hayden', 'marion.hayden@everyma1l.biz', '1955-04-26', 'Dundee Road', '9622', 'Cumming');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Brian', 'English', 'brian.english@gma1l.net', '1976-03-08', 'Raven Lane', '2212', 'Graham');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Jordan', 'Gaines', 'jordan.gaines@gma1l.us', '1966-03-13', 'Partridge Rd', '0028', 'Greenville');
insert into student (first_name, last_name, email, date_of_birth, address, zip, city)
values ('Andy', 'Harding', 'andy.harding@everyma1l.com', '1982-03-01', 'Gilbert Park', '6622', 'Canton');

